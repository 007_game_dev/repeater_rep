package repeater.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import repeater.game.MyGame;
import repeater.game.objects.Textboard;

/**
 * Created by alex on 22.01.2017.
 */

public class LoseScreenState extends State {

    private static final String HEADER = "Game Over";
    private static final float HEADER_POS_X = MyGame.WIDTH/2;
    private static final float HEADER_POS_Y = MyGame.HEIGHT/2 + 100;
    private Texture background;
    private boolean acientTap;
    private Textboard tb;
    private Textboard totalScore;
    private Textboard gameSize;
    private Textboard gameDiff;

    public LoseScreenState(GameStateManager gsm, int total_score, int N, int M, int Dif) {
        super(gsm);
        camera.setToOrtho(false, MyGame.WIDTH, MyGame.HEIGHT);
        background = new Texture("red_background.png");
        acientTap = true;
        tb = new Textboard(MyGame.HEADER_FONT,HEADER,HEADER_POS_X, HEADER_POS_Y, Color.WHITE);
        totalScore = new Textboard(MyGame.BASIC_FONT,"Total score: " + total_score, HEADER_POS_X, HEADER_POS_Y - 50, Color.WHITE);
        gameSize = new Textboard(MyGame.SMALL_FONT,"Size: " + N + "x" + M, HEADER_POS_X, HEADER_POS_Y - 100, Color.WHITE);
        gameDiff = new Textboard(MyGame.SMALL_FONT,"Difficulty: " + Dif, HEADER_POS_X, HEADER_POS_Y - 130, Color.WHITE);
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.isTouched())
        {
            if(!acientTap) {
                gsm.set(new MenuState(gsm));
            }
        }
        else
        {
            acientTap = false;
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {

        sb.setProjectionMatrix(camera.combined);
        sb.draw(background, 0, 0, MyGame.WIDTH,MyGame.HEIGHT);
        tb.draw(sb);
        totalScore.draw(sb);
        gameSize.draw(sb);
        gameDiff.draw(sb);
    }

    @Override
    public void dispose() {
        background.dispose();
        tb.dispose();
        gameSize.dispose();
        gameDiff.dispose();
    }
}
