package repeater.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.Random;

import repeater.game.MyGame;
import repeater.game.objects.AreaOfPanels;
import repeater.game.objects.Button;
import repeater.game.objects.Panel;
import repeater.game.objects.Textboard;

/**
 * Created by alex on 21.01.2017.
 */

public class EmptyLevelState extends State {
    private Texture UserTurnBackground, ShowTurnBackground;
    private int i;
    //работа с панелями уровня
    private AreaOfPanels aPanels;
    private static final float LEFT_INDENT = 50;
    private static final float BOTTOM_INDENT = 150;
    private static final float PANELS_INDENT = 10;
    private int N = 4;
    private int M = 3;
    //работа с демонстрацией ходов
    private int NUMBER_PANELS_FOR_SHOW = 2; //константа на кол-во отображаемых будующих шагов просто приравнивается к NumberPanelsOfShow
    private static final float SHOW_PANEL_TIME = 1; //константа времени отображения одной панели (сколько она будет "гореть")
    private float CurShowTime; //текущее время "горения" панели (при привышении SHOW_PANEL_TIME "гасим" её)
    private int CurPanel; // текущая панель, которая горит
    private int NumberPanelsOfShow; //текущее колво панелей для отображения, если = 0, то можно дать управление пользователю
    private com.badlogic.gdx.utils.Queue<Integer> qPanels; // очередь, в которой сохранена последовательность отображённых панелей (пользователю же надо повторить)
    private Random rand; // переменная рандома, чтоб зарандомить отображение панелей
    //
    private int score;
    private static final String SCORE_HEADER = "Score: ";
    private Button tb;


    public EmptyLevelState(GameStateManager gsm, int N, int M, int NUMBER_PANELS_FOR_SHOW) {
        super(gsm);
        this.N = N;
        this.M = M;
        this.NUMBER_PANELS_FOR_SHOW = NUMBER_PANELS_FOR_SHOW;
        camera.setToOrtho(false, MyGame.WIDTH, MyGame.HEIGHT);

        UserTurnBackground = new Texture("white_background.png");
        ShowTurnBackground = new Texture("white_background2.png");


        aPanels = new AreaOfPanels(new Panel(new Texture("gray.png"),
                new Texture("green_1.png"),
                new Texture("red_1.png"),
                new Rectangle(1,1,1,1)),N,M,
                new Rectangle(LEFT_INDENT,BOTTOM_INDENT, MyGame.WIDTH - 2*LEFT_INDENT, MyGame.HEIGHT - 2*BOTTOM_INDENT),PANELS_INDENT);

        //объявления всякой всячины
        score = 0;
        tb = new Button(MyGame.BASIC_FONT,SCORE_HEADER + score,Color.WHITE,new Texture("red_1.png"), new Texture("red_2.png"),
                new Rectangle( MyGame.WIDTH/2 - 75, MyGame.HEIGHT - BOTTOM_INDENT/2 - 30, 150, 60));
        rand = new Random();
        qPanels = new com.badlogic.gdx.utils.Queue<Integer>();
        CurShowTime = 0;
        NumberPanelsOfShow = NUMBER_PANELS_FOR_SHOW;
    }

    @Override
    //метод проверки для тыков по экрану
    protected void handleInput() {
        if(Gdx.input.isTouched())
        {
            mouse.set(Gdx.input.getX(),Gdx.input.getY(), 0);
            camera.unproject(mouse);

        }
        else
        {
            mouse.x = -1;
            mouse.y = -1;
        }


    }

    //обработка уровня при пользовательском ходе
    public void UserTurn(float dt) {
        handleInput();

        aPanels.update(mouse);
        //обновление панелей
        i = aPanels.getNumberPressedPunel();
        if (i>=0 && i == qPanels.first()) {
            //если была нажата верная панель
            aPanels.update(mouse);
            qPanels.removeFirst();
            score++;
            tb.setText(SCORE_HEADER+score);
            NumberPanelsOfShow++;

        }
        else {
            if (i >= 0) {
                //если была нажата неверная панель
                gsm.set(new LoseScreenState(gsm, score, N, M, NUMBER_PANELS_FOR_SHOW));
            }
        }
    }

    //обработка уровня при демонстрации ходов
    public void ShowTurn(float dt)
    {
        if(CurShowTime == 0)
        {
            CurPanel = rand.nextInt(N*M);
            aPanels.showPanel(CurPanel);
        }
        if(CurShowTime < SHOW_PANEL_TIME)
        {
            CurShowTime += dt;
        }
        else
        {
            qPanels.addLast(CurPanel);
            aPanels.hidePanel(CurPanel);
            NumberPanelsOfShow--;
            CurShowTime = 0;
        }
    }

    @Override
    public void update(float dt) {
        //если есть что показать, то показываем
        if(NumberPanelsOfShow > 0)
        {
            ShowTurn(dt);
        }
        else // если нет, то даём ход пользователю
        {
            UserTurn(dt);
        }

    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(camera.combined);
        //фон в зависимости от хода
        if(NumberPanelsOfShow > 0)
        {
            sb.draw(ShowTurnBackground,0,0,MyGame.WIDTH,MyGame.HEIGHT);
        }
        else
        {
            sb.draw(UserTurnBackground,0,0,MyGame.WIDTH,MyGame.HEIGHT);
        }

        //табло
        tb.draw(sb);

        //прорисовка панелей
        aPanels.render(sb);
    }

    @Override
    public void dispose() {

        aPanels.dispose();
        qPanels.clear();
        tb.dispose();
    }
}
