package repeater.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

/**
 * Created by alex on 21.01.2017.
 */

public class GameStateManager {
    private Stack<State> states;
    public GameStateManager(){
        states = new Stack<State>();
    }

    //запихивание
    public void push(State state){
        states.push(state);
    }

    //удаление
    public void pop(){
        states.pop().dispose();
    }

    //замена эелемента
    public void set(State state){
        states.pop().dispose();
        states.push(state);
    }

    //вызов update из верхнего состояния
    public void update(float dt){
        states.peek().update(dt);
    }

    //вызов render из верхнего состояния
    public void render(SpriteBatch sb){
        states.peek().render(sb);
    }
}
