package repeater.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import repeater.game.MyGame;
import repeater.game.objects.Button;
import repeater.game.objects.DifficultySwither;
import repeater.game.objects.SizeAreaSwither;


/**
 * Created by alex on 21.01.2017.
 */

public class MenuState extends State {

    public static final float BUTTON_WIDTH = 120;
    public static final float BUTTON_HEIGHT = 60;
    public static final float BUTTON_X = MyGame.WIDTH/2;
    public static final float BUTTON_Y = MyGame.HEIGHT/2 + 120;
    private Button button;
    private boolean acientTap;
    private SizeAreaSwither sas;
    private DifficultySwither ds;

    public MenuState(GameStateManager gsm) {
        super(gsm);
        button = new Button(MyGame.BASIC_FONT,"START", Color.WHITE,new Texture("red_1.png"), new Texture("red_2.png"),
                new Rectangle(BUTTON_X - BUTTON_WIDTH/2,BUTTON_Y - BUTTON_HEIGHT/2,BUTTON_WIDTH,BUTTON_HEIGHT));
        camera.setToOrtho(false, MyGame.WIDTH, MyGame.HEIGHT);
        acientTap = true;
        sas = new SizeAreaSwither();
        ds = new DifficultySwither();
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.isTouched())
        {
            if(!acientTap) {
                mouse.set(Gdx.input.getX(), Gdx.input.getY(), 0);
                camera.unproject(mouse);
            }
        }
        else
        {
            acientTap = false;
            mouse.x = -1;
            mouse.y = -1;
        }
        //обновляем состояние кнопки
        button.UpdateStateButton(mouse);
    }

    @Override
    public void update(float dt) {
        handleInput();
        if(button.Clicked())
        {
            gsm.set(new EmptyLevelState(gsm, sas.getN(),sas.getM(), ds.getDif()));
        }
        sas.update(mouse);
        ds.update(mouse);
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(camera.combined);
        button.draw(sb);
        sas.draw(sb);
        ds.draw(sb);
    }

    @Override
    public void dispose() {
        ds.dispose();
        button.dispose();
        sas.dispose();
    }
}
