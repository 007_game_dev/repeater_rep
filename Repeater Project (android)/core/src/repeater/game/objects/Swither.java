package repeater.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Queue;

/**
 * Created by alex on 24.01.2017.
 */

public class Swither {

    private Button next, prev;
    private Button body;
    private Queue<String> qText;

    public Swither(Button body, Button next, Button prev)
    {
        this.next= next;
        this.prev = prev;
        this.body = body;
        qText = new Queue<String>();
    }

    public void Push(String text)
    {
        qText.addLast(text);
        if(qText.size == 1)
        {
            body.setText(qText.first());
        }
    }

    public void removeCurrent()
    {
        qText.removeFirst();
    }

    public String getText()
    {
        return qText.first();
    }

    public void removeAll()
    {
        qText.clear();
    }

    public void switchNext()
    {
        qText.addLast(qText.first());
        qText.removeFirst();
        body.setText(qText.first());
    }

    public void switchPrev()
    {
        qText.addFirst(qText.last());
        qText.removeLast();
        body.setText(qText.first());
    }


    public void update(Vector3 mouse)
    {
        next.UpdateStateButton(mouse);
        prev.UpdateStateButton(mouse);
        if(next.Clicked())
        {
            switchNext();
        }
        if(prev.Clicked())
        {
            switchPrev();
        }

    }

    public void draw(SpriteBatch sb)
    {
        body.draw(sb);
        next.draw(sb);
        prev.draw(sb);
    }

    public void dispose()
    {
        //qText.clear();
        body.dispose();
        next.dispose();
        prev.dispose();
    }
}
