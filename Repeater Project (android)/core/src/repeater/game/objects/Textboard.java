package repeater.game.objects;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Align;

import repeater.game.MyGame;

/**
 * Created by alex on 23.01.2017.
 */

public class Textboard {

    private String txt;
    public BitmapFont font;
    private GlyphLayout layout;
    private Color color;
    public float x,y;
    public Textboard(BitmapFont font,String Text, float x, float y, Color col) {
        color = col;
        this.x = x;
        this.y = y;
        this.font = font;

        layout = new GlyphLayout();
        txt = Text;
        setText(Text);
    }
    public String getText()
    {
        return txt;
    }

    public void setText(String text)
    {
        txt = text;
        layout.setText(font,txt, color, 0, Align.center,false);
    }
    public void draw(SpriteBatch sb)
    {
        font.draw(sb,layout,x, y + layout.height/2);
    }
    public void dispose()
    {
    }
}
