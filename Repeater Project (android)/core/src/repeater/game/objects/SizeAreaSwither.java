package repeater.game.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import repeater.game.MyGame;

/**
 * Created by alex on 24.01.2017.
 */

public class SizeAreaSwither {

    private static final float X_POS = MyGame.WIDTH/2;
    private static final float Y_POS = MyGame.HEIGHT/2;
    private static final float BODY_WIDTH = 100;
    private static final float BODY_HEIGHT = 50;
    private static final float BUTTONS_WIDTH = 35;
    private static final float BUTTONS_HEIGHT = 50;
    private static final float INDENT = 10;
    private Swither sw;

    public SizeAreaSwither() {
        sw = new Swither(
                new Button(MyGame.BASIC_FONT,"", Color.WHITE,new Texture("switcher_body.png"),new Texture("switcher_body.png"),new Rectangle(X_POS - BODY_WIDTH/2, Y_POS - BODY_HEIGHT/2,BODY_WIDTH,BODY_HEIGHT)),
                new Button(MyGame.BASIC_FONT,"", Color.WHITE,new Texture("switcher_right_up.png"),new Texture("switcher_right_down.png"),new Rectangle(X_POS + BODY_WIDTH/2 + INDENT, Y_POS - BUTTONS_HEIGHT/2,BUTTONS_WIDTH,BUTTONS_HEIGHT)),
                new Button(MyGame.BASIC_FONT,"", Color.WHITE,new Texture("switcher_left_up.png"),new Texture("switcher_left_down.png"),new Rectangle(X_POS - BODY_WIDTH/2 - INDENT - BUTTONS_WIDTH, Y_POS - BUTTONS_HEIGHT/2,BUTTONS_WIDTH,BUTTONS_HEIGHT)));
        sw.Push("2x2");
        sw.Push("3x2");
        sw.Push("3x3");
        sw.Push("4x3");
    }

    public int getN()
    {
        switch(sw.getText())
        {
            case "2x2": return 2;
            case  "3x2": return 3;
            case  "3x3": return 3;
            case  "4x3": return 4;
            default:
                return 2;
        }
    }

    public int getM()
    {
        switch(sw.getText())
        {
            case "2x2": return 2;
            case  "3x2": return 2;
            case  "3x3": return 3;
            case  "4x3": return 3;
            default:
                return 2;
        }
    }

    public void update(Vector3 mouse)
    {
        sw.update(mouse);
    }

    public void draw(SpriteBatch sb)
    {
        sw.draw(sb);
    }

    public void dispose()
    {
        sw.dispose();
    }


}
