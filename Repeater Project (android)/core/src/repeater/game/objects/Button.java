package repeater.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by alex on 21.01.2017.
 */

public class Button {

    private Textboard textboard;
    private Texture upTexture;
    private Texture downTexture;
    public Rectangle rect;
    public Boolean stateButton;
    //отслеживают изменения состояния
    public boolean ChangeToDown;
    public boolean ChangeToUp;

    public Button(BitmapFont font, String text, Color col,Texture up, Texture down, Rectangle rect) {
        upTexture = up;
        downTexture = down;
        this.rect = rect;
        stateButton = false;
        ChangeToDown = false;
        ChangeToUp = false;

        textboard = new Textboard(font,text,rect.x + rect.width/2,rect.y + rect.height/2, col);
    }

    public void UpdateStateButton(Vector3 mouse_pos) {
        if(mouse_pos.x >= rect.x && mouse_pos.x <= rect.x +  rect.width
                && mouse_pos.y >= rect.y && mouse_pos.y <= rect.y +  rect.height)
        {
            if(!stateButton)
            {
                ChangeToDown = true;
            }
            else
            {
                ChangeToDown = false;
            }
            stateButton = true;
        }
        else {
            if(stateButton)
            {
                ChangeToUp = true;
            }
            else
            {
                ChangeToUp = false;
            }
            stateButton = false;
        }
    }

    public boolean Clicked()
    {
        if(!Gdx.input.isTouched() && ChangeToUp)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Texture GetCurTexture() {
        if(stateButton) {
            return downTexture;
        }
        else
        {
            return  upTexture;
        }
    }

    public void draw(SpriteBatch sb)
    {
        sb.draw(GetCurTexture(), rect.x,rect.y,rect.width,rect.height);
        textboard.draw(sb);
    }

    public void setText(String text)
    {
        textboard.setText(text);
    }

    public String getText()
    {
        return textboard.getText();
    }

    public void dispose() {
        upTexture.dispose();
        downTexture.dispose();
        textboard.dispose();
    }

}
