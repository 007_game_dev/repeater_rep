package repeater.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by alex on 22.01.2017.
 */

public class Panel {

    private Texture stayTexture;
    private Texture trueTexture;
    private Texture falseTexture;
    public Boolean pressState; // хранит состояние нажатия
    public Boolean choiceState; //хранит состояние для нажатия (выдаёт соответствующую текстуру после нажатия)
    public Rectangle rect;
    //отслеживают изменения состояния
    public boolean ChangeToDown;
    public boolean ChangeToUp;

    public Panel(Panel p)
    {
        stayTexture = p.stayTexture;
        trueTexture = p.trueTexture;
        falseTexture = p.falseTexture;
        pressState = p.pressState;
        choiceState = p.choiceState;
        rect = new Rectangle(p.rect);
        ChangeToDown = false;
        ChangeToUp = false;
    }

    public Panel(Texture stayTexture, Texture trueTexture, Texture falseTexture, Rectangle rect) {
        this.stayTexture = stayTexture;
        this.trueTexture = trueTexture;
        this.falseTexture = falseTexture;
        this.rect = rect;
        pressState = false;
        choiceState = true;
        ChangeToDown = false;
        ChangeToUp = false;
    }

    public void UpdateStateButton(Vector3 mouse_pos) {
        if(mouse_pos.x >= rect.x && mouse_pos.x <= rect.x +  rect.width
                && mouse_pos.y >= rect.y && mouse_pos.y <= rect.y +  rect.height)
        {
            if(!pressState)
            {
                ChangeToDown = true;
            }
            else
            {
                ChangeToDown = false;
            }
            pressState = true;
        }
        else {
            if(pressState)
            {
                ChangeToUp = true;
            }
            else
            {
                ChangeToUp = false;
            }
            pressState = false;
        }
    }

    public boolean Clicked()
    {
        if(ChangeToDown)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Texture GetCurTexture() {
        if(pressState) {
            if(choiceState) {
                return trueTexture;
            }
            else
            {
                return falseTexture;
            }
        }
        else
        {
            return  stayTexture;
        }
    }

    public void draw(SpriteBatch sb)
    {
        sb.draw(GetCurTexture(), rect.x,rect.y,rect.width,rect.height);
    }

    public void dispose() {
        stayTexture.dispose();
        trueTexture.dispose();
        falseTexture.dispose();
    }

}
