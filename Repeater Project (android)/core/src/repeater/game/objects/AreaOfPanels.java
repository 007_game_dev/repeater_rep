package repeater.game.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import repeater.game.MyGame;

/**
 * Created by alex on 24.01.2017.
 */

public class AreaOfPanels {

    private Panel panels[]; // NxM
    private int n; //кол-во строк с панелями
    private int m; //кол-во столбцов с панелями
    private Rectangle rect;
    private float panels_indent; // отступы между панелями
    private int i;

    public AreaOfPanels(Panel p,int n, int m, Rectangle rect, float panels_indent) {
        this.n = n;
        this.m = m;
        this.panels_indent = panels_indent;
        this.rect = rect;
        panels = new Panel[n*m];
        float curWidth = (rect.width - (m - 1)*panels_indent)/m;
        float curHeight = (rect.height - (n - 1) * panels_indent)/n;
        for( i = 0; i < n; i++) {
            for(int j = 0; j < m; j++)
            {
                panels[i*m+j] = new Panel(p);
                //panels[i*m+j].rect = new Rectangle()
                panels[i*m+j].rect.y = rect.y + (curHeight + panels_indent)*i;
                panels[i*m+j].rect.x = rect.x + (curWidth + panels_indent)*j;
                panels[i*m+j].rect.height = curHeight;
                panels[i*m+j].rect.width = curWidth;
            }
        }
    }

    public void update(Vector3 mouse)
    {
        for( i = 0; i < n*m; i++) {
            panels[i].UpdateStateButton(mouse);
        }
    }

    public int getNumberPressedPunel()
    {
        for( i = 0; i < n*m; i++) {
            if(panels[i].Clicked()) {
                return i;
            }
        }
        return -1;
    }

    public void showPanel(int number)
    {
        panels[number].choiceState = false;
        panels[number].pressState = true;
    }

    public void hidePanel(int number)
    {
        panels[number].choiceState = true;
        panels[number].pressState = false;
    }

    public void render(SpriteBatch sb)
    {
        for( i = 0; i < n*m; i++) {
            panels[i].draw(sb);
        }
    }

    public void dispose()
    {
        for( i = 0; i < n*m; i++) {
            panels[i].dispose();
        }
    }
}
